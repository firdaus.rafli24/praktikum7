import java.lang.*;
import java.awt.Color;

public class Kucing
{
	private String nama;
	private int usia;
	private double beratBadan;
	private boolean statusJinak;
	private String majikan;
	
	
	public void setName(String temp){
	
		nama = temp;
	}
	
	public String getName(){
	
		return nama;
	}
	
	public void setUsia(int temp){
		
		usia = temp;
	}
	
	public int getUsia(){
	
		return usia;
	}

	public void setBb(double temp){
	
		beratBadan = temp;
	}
	
	public double getBb(){
	
		return beratBadan;
	}

	public void setMajikan(String temp){
	
		majikan = temp;
	}
	
	public String getMajikan(){
		
		return majikan;
	}

	/*public void setStatusJinak(){
		
		boolean temp = false;
		if(majikan != " "){
			
			temp = true;
		}
		
		statusJinak = temp;
	}
	*/	
	
	public boolean getStatusJinak(){

		boolean temp = false;
		if(majikan != " "){
			
			temp = true;
		}
		
		statusJinak = temp;
		return statusJinak;
	}	
	
}